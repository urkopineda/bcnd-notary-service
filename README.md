# Blockchain Nanodegree - Build a Private Blockchain Notary Service

Welcome to my project! This Node.js + Express server hosts a private notary service in which you can add your stars.

Run the server using `npm start`.

Here are the available targets on the server:

*   **REQUEST VALIDATION:** Make a [request validation](/requestValidation) adding as POST your Blockchain ID address (Example: `{ "address": "142BDCeSGbXjWKaAnYXbMpZ6sbrSAo3DpZ" }`)
*   **VALIDATE MESSAGE:** Validate your request validation validating your request with a [message signature](/message-signature/validate), adding as POST your Blockchain ID address and a message signature (Example: `{ "address": "142BDCeSGbXjWKaAnYXbMpZ6sbrSAo3DpZ", "signature": "H6ZrGrF0Y4rMGBMRT2+hHWGbThTIyhBS0dNKQRov9Yg6GgXcHxtO9GJN4nwD2yNXpnXHTWU9i+qdw5vpsooryLU=" }`)
*   **ADD YOUR STAR:** After making your request validation and validating a message with your signature, you can [add a star](/block), adding as POST your star data (Example: `{ "address": "142BDCeSGbXjWKaAnYXbMpZ6sbrSAo3DpZ", "star": { "dec": "-26° 29' 24.9", "ra": "16h 29m 1.0s", "story": "Found star using https://www.google.com/sky/" } }`)
*   **GET STAR BY OPTIONS:** Get a star by options like `hash:HASH` and `address:ADDRESS` (Example `http://localhost:8000/stars/address:12Kd6Hhpe5aoy2GZb1tmZCjMwz2q2TGs7R`)
*   **GET STAR BY HEIGHT:** Get a star by height  (Example `http://localhost:8000/block/3`)

Additionally, you can check **postman_collection.json file** with a valid example with requests and results

Regards!

Urko
