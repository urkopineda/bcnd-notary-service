// Imports
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const NotaryService = require('./src/notaryService').NotaryService;
// Constant values
const port = process.env.PORT || 8000;
const notaryService = new NotaryService();

// Set body parser to get POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
// Welcome page
app.get('/', (req, res) => res.sendFile(`${__dirname}/index.html`));
// Get a block given its height
app.get('/block/:height', async (req, res) => {
  try {
    let height = await notaryService.getBlockHeight();
    if (height === -1) {
      res.status(500);
      res.send('ERROR: At this moment, there is no block on the chain, try adding one');
    } else {
      let block = await notaryService.getStarHeigh(req.params.height);
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(block));
    }
  } catch (error) {
    res.status(500);
    res.send(error.message);
  }
});
// Get a star given an option
app.get('/stars/:options', async (req, res) => {
  try {
    let height = await notaryService.getBlockHeight();
    if (height === -1) {
      res.status(500);
      res.send('ERROR: At this moment, there is no block on the chain, try adding one');
    } else {
      let block = await notaryService.getStar(req.params.options);
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(block));
    }
  } catch (error) {
    res.status(500);
    res.send(error.message);
  }
});
// Add a star
app.post('/block', async (req, res) => {
  try {
    let data = req.body;
    if (data && data !== '') {
      let block = await notaryService.addBlockEntry(data);
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(block));
    } else {
      res.status(500);
      res.send('ERROR: You must POST a JSON with the key \'body\' with some value');
    }
  } catch (error) {
    res.status(500);
    res.send(error.message);
  }
});
// Request validation
app.post('/requestValidation', async (req, res) => {
  try {
    let data = req.body;
    if (data && data !== '') {
      let block = await notaryService.requestValidation(data);
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(block));
    } else {
      res.status(500);
      res.send('ERROR: You must POST a JSON with the key \'body\' with some value');
    }
  } catch (error) {
    res.status(500);
    res.send(error.message);
  }
});
// Check message signature
app.post('/message-signature/validate', async (req, res) => {
  try {
    let data = req.body;
    if (data && data !== '') {
      let block = await notaryService.validateMessage(data);
      res.status(200);
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(block));
    } else {
      res.status(500);
      res.send('ERROR: You must POST a JSON with the key \'body\' with some value');
    }
  } catch (error) {
    res.status(500);
    res.send(error.message);
  }
});
// Start Express server...
app.listen(port, () => console.info(`Project API listening on port ${port}...`));
