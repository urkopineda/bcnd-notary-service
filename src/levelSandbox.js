/* ===== Persist data with LevelDB ===================================
|  Learn more: level: https://github.com/Level/level     |
|  =============================================================*/

// Imports
const db = require('level')('./chaindata');

/**
 * Add a block of data
 * @param {any} value to be added
 */
module.exports.add = (value) => new Promise((resolve, reject) => {
  let i = 0;
  db.createReadStream().on('data', () => {
    i++;
  }).on('error', (error) =>{
    console.error('Unable to read data stream!', error);
    return reject(error);
  }).on('close', async () => {
    await module.exports.addKey(i, value);
    return resolve(i);
  });
});

/**
 * Add a block of data with a concrete key
 * @param {string} key to the block
 * @param {any}    value to be added
 */
module.exports.addKey = (key, value) => new Promise((resolve, reject) =>
  db.put(key, value)
    .then(() => resolve(true))
    .catch((error) => {
      console.error(`Block ${key} submission failed`, error);
      return reject(error);
    }));

/**
 * Update a block with a new data
 * @param {string} key to the value
 * @param {any}    value to be updated
 */
module.exports.update = (key, value) => new Promise((resolve, reject) =>
  db.put(key, value)
    .then(() => resolve(true))
    .catch((error) => {
      console.error(`Block ${key} edition failed`, error);
      return reject(error);
    }));

/**
 * Delete a key
 * @param {string} key to the value
 */
module.exports.delete = (key) => new Promise((resolve, reject) =>
  db.del(key)
    .then(() => resolve(true))
    .catch((error) => {
      console.error(`Block ${key} deletion failed`, error);
      return reject(error);
    }));

/**
 * Delete all levelDB data
 */
module.exports.deleteAll = () =>  new Promise((resolve, reject) => {
  let i = 0;
  db.createReadStream().on('data', () => {
    i++;
  }).on('error', (error) => {
    console.error('Unable to read data stream!', error);
    return reject(error);
  }).on('close', async () => {
    for (let j = 0; j < i; j++) {
      await module.exports.delete(j);
    }
    return resolve(true);
  });
});

/**
 * Get data from levelDB with key
 * @param   {string} key to the value
 * @returns {any}    value
 */
module.exports.get = (key) => new Promise((resolve, reject) =>
  db.get(key)
    .then((value) => resolve(value))
    .catch((error) => {
      console.error('Not found!', error);
      return reject(error);
    }));

/**
 * Get data length of the chain
 * @returns {number} data lenght
 */
module.exports.dataNumber = () => new Promise((resolve, reject) => {
  let i = 0;
  db.createReadStream().on('data', () => {
    i++;
  }).on('error', (error) => {
    console.error('Unable to read data stream!', error);
    return reject(error);
  }).on('close', () =>
    resolve(i - 1)
  );
});
