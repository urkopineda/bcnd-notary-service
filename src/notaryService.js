// Imports
const level = require('./levelSandbox');
const SHA256 = require('crypto-js/sha256');
const Message = require('bitcore-message');
// Constant values
const VALIDATION_WINDOW = 300;
const STORY_MAXIMUM_WORDS = 250;
const STORY_MAXIMUM_BYTES = 500;

/**
 * Simple block structure
 */
class Block {
  constructor(data) {
    this.hash = '';
    this.height = 0;
    this.body = data;
    this.time = 0;
    this.previousBlockHash = '';
  }
}

/**
 * This is the notary service class
 */
class NotaryService {
  constructor() {
    this.chain = level,
    this.processingGenesis = false
  }

  /**
   * Request validation to add a star to the registry
   * @param   {string} data of the user
   * @returns {Block}  result of the request validation
   */
  async requestValidation(data) {
    let parsedData = JSON.parse(JSON.stringify(data));
    // Check if provided data is valid
    if (parsedData && parsedData.address) {
      let block, oldBlock, addNew = false;
      let address = parsedData.address;
      let i = 0, chainHeight = await this.getBlockHeight() + 1;
      // Check if there is a request with the same Blockchain ID
      while (i < chainHeight) {
        let helper = await this.getBlock(i)
        if (helper.body.status && helper.body.status.address === address) {
          oldBlock = helper;
        }
        i++;
      }
      // If there is a request with the same Blockchain ID...
      if (oldBlock && !oldBlock.body.registerStarDone) {
        let requestTimeStamp = oldBlock.body.status.requestTimeStamp;
        let newValidationWindow = VALIDATION_WINDOW - Math.trunc((new Date() - new Date(Number(requestTimeStamp))) / 1000);
        // If the validation window is valid, update the validation window
        if (newValidationWindow > 0) {
          oldBlock.body.status.validationWindow = newValidationWindow;
          block = await this.addBlock(new Block(oldBlock.body));
        // If not, add a new block
        } else {
          addNew = true;
        }
      // If not, add a new block
      } else {
        addNew = true;
      }
      // If needed, add a new block
      if (addNew) {
        let requestTimeStamp = new Date().getTime();
        block = await this.addBlock(new Block({
          registerStar: false,
          status: {
            address,
            requestTimeStamp: requestTimeStamp.toString(),
            message: `${address}:${requestTimeStamp}:starRegistry`,
            validationWindow: VALIDATION_WINDOW,
            messageSignature: 'invalid'
          }
        }));
      }
      // Return data to the user
      return {
        address: block.body.status.address,
        requestTimeStamp: block.body.status.requestTimeStamp,
        message: block.body.status.message,
        validationWindow: block.body.status.validationWindow,
      };
    // If not, throw a error to the user
    } else {
      throw new Error('Invalid data provided, you should POST a JSON with a "address" property')
    }
  }

  /**
   * Validate message of the user
   * @param   {string} data of the user
   * @returns {Block}  result of validation
   */
  async validateMessage(data) {
    let parsedData = JSON.parse(JSON.stringify(data));
    // Check if provided data is valid
    if (parsedData && parsedData.address && parsedData.signature) {
      let block;
      let address = parsedData.address;
      let signature = parsedData.signature;
      let i = 0, chainHeight = await this.getBlockHeight() + 1;
      // Check if there is a request with the Blockchain ID provided
      while (i < chainHeight) {
        let helper = await this.getBlock(i)
        if (helper.body.status && helper.body.status.address === address) {
          block = helper;
        }
        i++;
      }
      // If the request exists...
      if (block && !block.body.registerStarDone) {
        let requestTimeStamp = block.body.status.requestTimeStamp;
        let newValidationWindow = VALIDATION_WINDOW - Math.trunc((new Date() - new Date(Number(requestTimeStamp))) / 1000);
        // If validation window is valid...
        if (newValidationWindow > 0) {
          // If validation of the signature is correct, add a new block with validity data
          if (Message(block.body.status.message).verify(address, signature)) {
            let newBlock = await this.addBlock(new Block({
              registerStar: true,
              status: {
                address,
                requestTimeStamp: requestTimeStamp,
                message: `${address}:${requestTimeStamp}:starRegistry`,
                validationWindow: newValidationWindow,
                messageSignature: 'valid'
              }
            }));
            return newBlock.body;
          // If not, tell the user that the signature is not valid
          } else {
            throw new Error('Signature verification error')
          }
        // If not, tell the user that the request is too old
        } else {
          throw new Error(`Your request validation is too old (validation window of ${VALIDATION_WINDOW} seconds)`)
        }
      // If not, tell the user to make a request
      } else {
        throw new Error('Your request validation doesn\'t exists on the DB or you have already registered a star, please make a new request')
      }
    // If not, throw a error to the user
    } else {
      throw new Error('Invalid data provided, you should POST a JSON with a "address" and "signature" property')
    }
  }

  /**
   * Add a star given it's data
   * @param   {string} data of the star
   * @returns {Block}  star data
   */
  async addBlockEntry(data) {
    const requiredKeys = ['address', 'star', 'star.dec', 'star.ra', 'star.story'];
    const parsedData = JSON.parse(JSON.stringify(data));
    // Check if provided data is valid
    for (let key of requiredKeys) {
      // If not, tell the user the keys that must be provided
      if (key.split('.').reduce((o, i) => o[i], parsedData) === undefined) {
        throw new Error('You must provide required properties of the star ("address", "star.dec", "star.ra", "star.story")');
      }
    }
    let block;
    let address = parsedData.address;
    let i = 0, chainHeight = await this.getBlockHeight() + 1;
    // Check if there is a request with the same Blockchain ID
    while (i < chainHeight) {
      let helper = await this.getBlock(i)
      if (helper.body.status && helper.body.status.address === address) {
        block = helper;
      }
      i++;
    }
    // If the request exists in the chain...
    if (block && !block.body.registerStarDone) {
      let requestTimeStamp = block.body.status.requestTimeStamp;
      let newValidationWindow = VALIDATION_WINDOW - Math.trunc((new Date() - new Date(Number(requestTimeStamp))) / 1000);
      // If validation window is valid...
      if (newValidationWindow > 0) {
        let story = parsedData.star.story;
        let buffer = Buffer.from(story , 'utf8');
        // If story limit is correct, add the star to the registry
        if (story.split(' ').length <= STORY_MAXIMUM_WORDS && buffer.byteLength <= STORY_MAXIMUM_BYTES) {
          parsedData.star.story = Buffer.from(parsedData.star.story , 'utf8').toString('hex');
          await this.addBlock(new Block({
            ...block.body,
            registerStarDone: true
          }))
          let newBlock = await this.addBlock(new Block(parsedData));
          return newBlock;
        // If not, tell the user that the story is too long
        } else {
          throw new Error(`Your star story is too long (maximum ${STORY_MAXIMUM_WORDS} words or ${STORY_MAXIMUM_BYTES} bytes)`)
        }
      // If not, tell the user that the request is too old
      } else {
        throw new Error(`Your request validation is too old (validation window of ${VALIDATION_WINDOW} seconds)`)
      }
    // If not, tell the user to make a request
    } else {
      throw new Error('Your request validation doesn\'t exists on the DB or you have already registered a star, please make a new request')
    }
  }

  /**
   * Get a star by options
   * @param   {string} data options
   * @returns {Block}  star
   */
  async getStar(data) {
    let regex = /([\w]+)\:([\w]+)/;
    let options = regex.exec(data);
    // Check if options are correct
    if (options && options.length) {
      let field = options[1];
      let value = options[2];
      let blocks = [];
      let i = 0, chainHeight = await this.getBlockHeight() + 1;
      // Look up for blocks that matches given options
      while (i < chainHeight) {
        let helper = await this.getBlock(i)
        // Check if provided options are valid and, if valid, check if block matches options
        switch(field) {
          case 'address':
            if (helper.body && helper.body.address && helper.body.address === value) {
              blocks.push(helper);
            }
            break;
          case 'hash':
            if (helper.hash && helper.hash === value) {
              blocks.push(helper);
            }
            break;
          default: throw new Error('Option invalid, only "address:ID" and "hash:HASH" are available')
        }
        i++;
      }
      // If there are blocks in the array
      if (blocks && blocks.length) {
        // Decode story
        for (let b of blocks) {
          b.body.star.storyDecoded = Buffer.from(b.body.star.story, 'hex').toString('utf8')
        }
        // Return blocks
        if (blocks.length === 1) {
          return blocks[0]
        } else {
          return blocks
        }
      // If not, notify the user
      } else throw new Error(`Star with options "${data}" not found`)
    // If not, notify the user
    } else throw new Error('Options or format are invalid, only "address:ID" and "hash:HASH" are available')
  }

  /**
   * Get a star by block height
   * @param   {number} blockHeight of the star
   * @returns {Block}  star
   */
  async getStarHeigh(blockHeight) {
    // Get the block
    let block = await this.getBlock(blockHeight);
    // If there is a block
    if (block) {
      // Check if the block is a star registration
      if (block && block.body && block.body.star) {
        // Decode story
        block.body.star.storyDecoded = Buffer.from(block.body.star.story, 'hex').toString('utf8');
        return block
      // If not, notify the user
      } else throw new Error(`Block with height "${blockHeight}" is not a star`)
    // If not, notify the user
    } else throw new Error(`Star with height "${blockHeight}" not found`)
  }

  // ##########################################################################################

  // Add new block
  async addBlock(newBlock) {
    let actualHeight = await this.getBlockHeight();
    // Check if there is a genesis block, if not, add it
    if (actualHeight === -1 && !this.processingGenesis) {
      this.processingGenesis = true;
      await this.addBlock(new Block('First block in the chain - Genesis block'));
      actualHeight = await this.getBlockHeight();
      this.processingGenesis = false;
    }
    // Block height
    newBlock.height = (actualHeight === -1) ? 0 : actualHeight + 1;
    // UTC timestamp
    newBlock.time = new Date().getTime().toString().slice(0, -3);
    // Previous block hash
    if (newBlock.height > 0) {
      let previousBlock = await this.getBlock(newBlock.height - 1);
      newBlock.previousBlockHash = previousBlock.hash;
    }
    // Block hash with SHA256 using newBlock and converting to a string
    newBlock.hash = SHA256(JSON.stringify(newBlock)).toString();
    // Adding block object to chain
    await this.chain.addKey(newBlock.height, JSON.stringify(newBlock).toString());
    // Return the block
    return newBlock;
  }

  // Get block height
  async getBlockHeight() {
    return await this.chain.dataNumber();
  }

  // Get block
  async getBlock(blockHeight) {
    // Return object as a single string
    return JSON.parse(await this.chain.get(blockHeight));
  }

  // Validate block
  async validateBlock(blockHeight) {
    // Get block object
    let block = await this.getBlock(blockHeight);
    // Get block hash
    let blockHash = block.hash;
    // Remove block hash to test block integrity
    block.hash = '';
    // Generate block hash
    let validBlockHash = SHA256(JSON.stringify(block)).toString();
    // Compare
    if (blockHash === validBlockHash) {
      return true;
    } else {
      console.warn(`Block #${blockHeight} invalid hash:\n${blockHash}<>${validBlockHash}`);
      return false;
    }
  }

  // Validate blockchain
  async validateChain() {
		let chainLength = await this.getBlockHeight();
    let errorLog = [];
    for (let i = 0; i < chainLength; i++) {
      // Validate block
      if (!await this.validateBlock(i)) errorLog.push(i);
      // Compare blocks hash link
      let block = await this.getBlock(i);
      let previousBlock = await this.getBlock(i + 1);
      let blockHash = block.hash;
      let previousHash = previousBlock.previousBlockHash;
      if (blockHash !== previousHash) {
        errorLog.push(i);
      }
    }
    if (errorLog.length > 0) {
      console.warn(`Block errors = ${errorLog.length}`);
      console.warn(`Block: ${errorLog}`);
			return false;
    } else {
			return true;
    }
  }
}

// Exports
module.exports.Block = Block;
module.exports.NotaryService = NotaryService;
